# ShopToken sample

Hey, guys here is the code of the interview test. As you guys requested I didn't look around for options of how to solve this, so keep that in mind when you review the code hehe.

## Requirements

The code requires a version of node higher than v14.x.

## How to run the test web app

Install dependencies
`npm i` or `yarn`.

Run project
`npm start` or `yarn start`

Browse the site at `http://localhost:3000`.

The page has two sections:

- on the left is a big `textarea` where the user can paste the list of categories as a string.
- on the right, there is a table that shows the items in a table.

To show the categories in the table click the `Sync table` button.

## Code structure

The repository contains the code to run the web app

```sh
├── public
│   # The html page and assets of the app
└── src
│   # Web App components.
├── components
│   ├── App
│   ├── Table
├──
│   # All server-side code
├── lib
│   │   # function to sort categories
│   ├── sortCategoriesForInsert
```


## Sorting strategy


The strategy used to sort the items was split into three steps:

step 1: load the items from the string into an array of categories.

In the code, it just parses the string into a collection, but this could be faster if the string (or maybe better a file) is converted into a stream and each category is ordered one at a time.

step 2: Sort the category list into a valid sequence.

To minimize the ordering iterations the way I thought about it is just iterating once the collection with a reducer and as it iterates it creates a new collection of categories sorted properly.

step 3: converting into a string the categories.
This stringifies the sorted categories into a string.

## UI structure

It uses `create-react-app` dependencies to simplify running the react app (like using `react-scripts`).

For the UI it uses chakra, which is a UI framework built on react that simplifies styling the site and adjusting the units/dimensions/etc.

For the Table, it uses `react-table` a react component that provides all the logic needed for rendering and customizing a table. In this case, we are just using the react hooks so we can render the table with chakra Table components. 

I couldn't complete the table component using pagination or virtual scroll to make the UI smoother, but I would have use something like that:

virtual scroll:
https://react-table.tanstack.com/docs/examples/virtualized-rows

with pagination:
https://react-table.tanstack.com/docs/api/usePagination


for filtering the rows it uses `use-debounce` which provides a debounce hook that can be used to filter the rows without impacting the search input field.
