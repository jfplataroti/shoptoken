import React, { useState, useCallback, useMemo } from "react";

// components
import {
  Button,
  Flex,
  Heading,
  Input,
  Textarea,
  Text,
  Box,
} from "@chakra-ui/react";
import { useDebounce } from "use-debounce";
import { Table } from "./Table";

// model / libs
import { Column } from "react-table";
import { generateCategories } from "../lib/generateContent";
import { Category } from "../lib/sortCategoriesForInsert";

// css
import "./App.css";

const columns = [
  {
    Header: "Id",
    accessor: "id",
  },
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Parent Id",
    accessor: "parentId",
  },
] as Column<Category>[];

function App() {
  const [text, setText] = useState(() =>
    JSON.stringify(generateCategories(10))
  );
  const [rows, setRows] = useState([]);

  const [searchInput, setSearchInput] = useState("");
  const [debounceSearchInput] = useDebounce(searchInput, 500);

  const updateText = useCallback(
    (e: React.ChangeEvent<HTMLTextAreaElement>) => setText(e.target.value),
    [setText]
  );

  const updateSearchInput = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => setSearchInput(e.target.value),
    [setSearchInput]
  );

  const sendToTable = useCallback(() => {
    setRows(JSON.parse(text));
  }, [text, setRows]);

  // this should be moved into somewhere else and the table should use pagination or virtual scrolling.
  const data = useMemo(() => {
    return rows.filter((row: Category) =>
      row.name.includes(debounceSearchInput)
    );
  }, [rows, debounceSearchInput]);

  return (
    <div>
      <Flex
        bg="gray.100"
        w="100%"
        p={4}
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Heading size="sm">shoptoken sample</Heading>
        <Input
          bg="white"
          w="md"
          value={searchInput}
          onChange={updateSearchInput}
          placeholder="Search in table"
        ></Input>
        <Button onClick={sendToTable} colorScheme="teal">
          Sync Table
        </Button>
      </Flex>
      <Flex>
        <Box className="category-json-wrapper">
          <Text p="2">JSON categories:</Text>
          <Textarea
            value={text}
            onChange={updateText}
            className="text-area"
            height="100%"
          />
        </Box>
        <Box w="100%">
          <Table columns={columns} data={data} />
        </Box>
      </Flex>
    </div>
  );
}

export default App;
