import { Category } from "./sortCategoriesForInsert";

/**
 * it validates the categories input string with the result string to make sure the result is valid.
 * @param inputText 
 * @param result 
 * @returns 
 */
export function validateCategorySequence(
  inputText: string,
  result: string
): boolean {
  const list = JSON.parse(result) as Category[];
  const inputList = JSON.parse(inputText) as Category[];

  if (list.length !== inputList.length) {
    throw new Error(
      `input list of length ${inputList.length} is different than the result ${list.length}`
    );
  }

  const ids = new Set();
  return list.every((item: Category) => {
    if (item.parentId && !ids.has(item.parentId)) {
      throw new Error(
        `item ${JSON.stringify(
          item
        )} parentId was not found bin ids ${JSON.stringify(ids.values())}`
      );
    }

    ids.add(item.id);

    return true;
  });
}
