export interface Category {
  id: number;
  parentId?: number;
  name: string;
}

export function sortCategoriesForInsert(input: string): string {
  const categories = JSON.parse(input);
  const sortedCategories = categories.reduce(sortCategoriesByInsertMode, []);
  return JSON.stringify(sortedCategories);
}

function sortCategoriesByInsertMode(
  list: Category[],
  category: Category
): Category[] {
  let index = list.findIndex((item) => item.parentId === category.id);
  if (index >= 0) {
    return [...list.slice(0, index - 1), category, ...list.slice(index)];
  }

  list.push(category);
  return list;
}
