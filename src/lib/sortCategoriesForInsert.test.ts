import { Category, sortCategoriesForInsert } from "./sortCategoriesForInsert";
import { validateCategorySequence } from "./validate";

describe("Test Sort Categories function", () => {
  it("Should work with an empty list", () => {
    const list: Category[] = [];
    const input = JSON.stringify(list);

    const result = sortCategoriesForInsert(input);
    expect(validateCategorySequence(input, result)).toBeTruthy();
  });

  it("Should sort a list without parent ids", () => {
    const list: Category[] = [
      { id: 1, name: "c1" },
      { id: 2, name: "c2" },
    ];
    const input = JSON.stringify(list);

    const result = sortCategoriesForInsert(input);
    expect(validateCategorySequence(input, result)).toBeTruthy();
  });

  it("Should reorder the items moving up the parent to the top", () => {
    const list: Category[] = [
      { id: 2, name: "c2", parentId: 1 },
      { id: 1, name: "c1" },
    ];
    const input = JSON.stringify(list);

    const result = sortCategoriesForInsert(input);
    expect(validateCategorySequence(input, result)).toBeTruthy();
  });

  it("Should work with a single item", () => {
    const list: Category[] = [{ id: 2, name: "c2" }];
    const input = JSON.stringify(list);

    const result = sortCategoriesForInsert(input);
    expect(validateCategorySequence(input, result)).toBeTruthy();
  });

  it("Should keep the same order if items are already in the correct order", () => {
    const list: Category[] = [
      { id: 1, name: "c1" },
      { id: 2, name: "c2", parentId: 1 },
      { id: 3, name: "c3", parentId: 1 },
      { id: 4, name: "c4", parentId: 2 },
      { id: 5, name: "c5" },
    ];
    const input = JSON.stringify(list);

    const result = sortCategoriesForInsert(input);
    expect(validateCategorySequence(input, result)).toBeTruthy();
  });

  it("Should reorder the categories in a insert order", () => {
    const list: Category[] = [
      { id: 3, name: "c3", parentId: 2 },
      { id: 2, name: "c2" },
      { id: 1, name: "c1" },
      { id: 5, name: "c5" },
      { id: 4, name: "c4", parentId: 3 },
    ];
    const input = JSON.stringify(list);

    const result = sortCategoriesForInsert(input);
    expect(validateCategorySequence(input, result)).toBeTruthy();
  });
});
