import { Category } from "./sortCategoriesForInsert";

/**
 * Generates random categories.
 */
export function generateCategories(total = 5) {
  return new Array<Category>(total)
    .fill({ id: 1, name: "fill" })
    .map((c, i) => ({ id: i, name: `c${i}` }));
}
